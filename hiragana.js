var hiragana = {
    "base_set":
    {
        "base": [
            {
                "_question": "あ",
                "_answer": "a"
            },
            {
                "_question": "い",
                "_answer": "i"
            },
            {
                "_question": "う",
                "_answer": "u"
            },
            {
                "_question": "え",
                "_answer": "e"
            },
            {
                "_question": "お",
                "_answer": "o"
            }
        ],
    },
    "KG_set":
    {
        "base": [
            {
                "_question": "か",
                "_answer": "ka"
            },
            {
                "_question": "き",
                "_answer": "ki"
            },
            {
                "_question": "く",
                "_answer": "ku"
            },
            {
                "_question": "け",
                "_answer": "ke"
            },
            {
                "_question": "こ",
                "_answer": "ko"
            }
        ],
    },
    "SZ_set":
    {
        "base": [
            {
                "_question": "さ",
                "_answer": "sa"
            },
            {
                "_question": "し",
                "_answer": "shi"
            },
            {
                "_question": "す",
                "_answer": "su"
            },
            {
                "_question": "せ",
                "_answer": "se"
            },
            {
                "_question": "そ",
                "_answer": "so"
            },
        ],
        "diacritics": [
            {
                "_question": "ざ",
                "_answer": "za"
            },
            {
                "_question": "じ",
                "_answer": "ji"
            },
            {
                "_question": "ず",
                "_answer": "zu"
            },
            {
                "_question": "ぜ",
                "_answer": "ze"
            },
            {
                "_question": "ぞ",
                "_answer": "zo"
            }
        ],
    },
    "TD_set":
    {
       "base": [
            {
                "_question": "た",
                "_answer": "ta"
            },
            {
                "_question": "ち",
                "_answer": "chi"
            },
            {
                "_question": "つ",
                "_answer": "tsu"
            },
            {
                "_question": "て",
                "_answer": "te"
            },
            {
                "_question": "と",
                "_answer": "to"
            }
        ],
        "diacritics": [
            {
                "_question": "だ",
                "_answer": "da"
            },
            {
                "_question": "ぢ",
                "_answer": "ji"
            },
            {
                "_question": "づ",
                "_answer": "zu"
            },
            {
                "_question": "で",
                "_answer": "de"
            },
            {
                "_question": "ど",
                "_answer": "do"
            }
        ]
    },
    "N_set":
    {
        "base": [
            {
                "_question": "な",
                "_answer": "na"
            },
            {
                "_question": "に",
                "_answer": "ni"
            },
            {
                "_question": "ぬ",
                "_answer": "nu"
            },
            {
                "_question": "ね",
                "_answer": "ne"
            },
            {
                "_question": "の",
                "_answer": "no"
            }
        ],
        "diatritics": [
            {
                "_question": "にゃ",
                "_answer": "nya"
            },
            {
                "_question": "にゅ",
                "_answer": "nyu"
            },
            {
                "_question": "にょ",
                "_answer": "nyo"
            }
        ]
    },
    "HBP_set":
    {
        "base": [
            {
                "_question": "は",
                "_answer": "ha"
            },
            {
                "_question": "ひ",
                "_answer": "hi"
            },
            {
                "_question": "ふ",
                "_answer": "fu"
            },
            {
                "_question": "へ",
                "_answer": "he"
            },
            {
                "_question": "ほ",
                "_answer": "ho"
            },
            {
                "_question": "ば",
                "_answer": "ba"
            },
            {
                "_question": "び",
                "_answer": "bi"
            },
            {
                "_question": "ぶ",
                "_answer": "bu"
            },
            {
                "_question": "べ",
                "_answer": "be"
            },
            {
                "_question": "ぼ",
                "_answer": "bo"
            },
            {
                "_question": "ぱ",
                "_answer": "pa"
            },
            {
                "_question": "ぴ",
                "_answer": "pi"
            },
            {
                "_question": "ぷ",
                "_answer": "pu"
            },
            {
                "_question": "ぺ",
                "_answer": "pe"
            },
            {
                "_question": "ぽ",
                "_answer": "po"
            }
        ]
    },
    "M_set":
    {
        "base": [
            {
                "_question": "ま",
                "_answer": "ma"
            },
            {
                "_question": "み",
                "_answer": "mi"
            },
            {
                "_question": "む",
                "_answer": "mu"
            },
            {
                "_question": "め",
                "_answer": "me"
            },
            {
                "_question": "も",
                "_answer": "mo"
            }
        ],
        "diatritics": [
            {
                "_question": "みゃ",
                "_answer": "mya"
            },
            {
                "_question": "みゅ",
                "_answer": "myu"
            },
            {
                "_question": "みょ",
                "_answer": "myo"
            }
        ]
    },
    "R_set":
    {
        "base": [
            {
                "_question": "ら",
                "_answer": "ra"
            },
            {
                "_question": "り",
                "_answer": "ri"
            },
            {
                "_question": "る",
                "_answer": "ru"
            },
            {
                "_question": "れ",
                "_answer": "re"
            },
            {
                "_question": "ろ",
                "_answer": "ro"
            }
        ],
        "diatritics": [
            {
                "_question": "りゃ",
                "_answer": "rya"
            },
            {
                "_question": "りゅ",
                "_answer": "ryu"
            },
            {
                "_question": "りょ",
                "_answer": "ryo"
            }
        ]
    },
    "Y_set":
    {
        "base": [
            {
                "_question": "や",
                "_answer": "ya"
            },
            {
                "_question": "ゆ",
                "_answer": "yu"
            },
            {
                "_question": "よ",
                "_answer": "yo"
            }
        ]
    },
    "WN_set":
    {
        "base": [
            {
                "_question": "わ",
                "_answer": "wa"
            },
            {
                "_question": "を",
                "_answer": "wo"
            },
            {
                "_question": "ん",
                "_answer": "n"
            },
        ]
    },
};
